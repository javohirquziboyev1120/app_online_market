package com.example.app_online_market_server.controller;

import com.example.app_online_market_server.payload.ApiResponse;
import com.example.app_online_market_server.payload.WarehouseDto;
import com.example.app_online_market_server.service.WarehouseService;
import com.example.app_online_market_server.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/warehouse")
public class WarehouseController {
    @Autowired
    WarehouseService warehouseService;

    @PostMapping
    public HttpEntity<?> addWarehouse(@RequestBody WarehouseDto dto){
        ApiResponse apiResponse = warehouseService.addWarehouse(dto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201:409).body(apiResponse);
    }
    @PutMapping
    public HttpEntity<?> editWarehouse(@RequestBody WarehouseDto dto){
        ApiResponse apiResponse = warehouseService.editWarehouse(dto);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.CREATED: HttpStatus.CONFLICT).body(apiResponse);
    }
    @GetMapping("/getPage")
    public HttpEntity<?> getWarhousePage(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
                                         @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size){
        return ResponseEntity.ok(warehouseService.getWarehousePage(page, size));
    }
    @GetMapping("getWarehouseList")
    public HttpEntity<?> getWarehouseList(){
        return ResponseEntity.ok(warehouseService.getList());
    }
}
