package com.example.app_online_market_server.controller;


import com.example.app_online_market_server.service.AttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/api/attachment")
public class AttachmentController {

    @Autowired
    AttachmentService attachmentService;

//    @Autowired
//    ExcelService excelService;
//    @Autowired
//    PdfService pdfService;

    //    @PreAuthorize("hasAnyAuthority('UPLOAD_FILE_ATTACHMENT')")
    @PostMapping("/upload")
    public HttpEntity<?> uploadFile(MultipartHttpServletRequest request) {
        return ResponseEntity.ok(attachmentService.uploadFile(request));
//                .ok(new String[]{
//                        ServletUriComponentsBuilder.
//                                fromCurrentContextPath()
//                                .path("/api/attachment/")
//                                .path(uuid.toString())
//                                .toUriString()
//                });
    }

    //    @PreAuthorize("hasAnyAuthority('GET_FILE_ATTACHMENT')")
    @GetMapping("/{id}")
    public HttpEntity<?> getFile(@PathVariable UUID id) {
        return attachmentService.getFile(id);
    }

    @GetMapping("/order/{id}")
    public void getOrderFile(@PathVariable UUID id, HttpServletResponse httpServletResponse) throws IOException {
        attachmentService.getOrderFile(id, httpServletResponse);
    }

//    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN','ROLE_SUPER_ADMIN')")
//    @GetMapping("/excel/agent")
//    public HttpEntity<?> getAgentListExcel() {
//        return excelService.downloadAgentList();
//    }
//
//    //    @PreAuthorize("hasAnyAuthority('PDF_EXCEL')")
//    @GetMapping("/excel/order/{id}")
//    public HttpEntity<?> getOrderListByAgent(@PathVariable UUID id) {
//        return excelService.downloadOrderListByAgent(id);
//    }
//
//    //    @PreAuthorize("hasAnyAuthority('PDF_EXCEL')")
//    @GetMapping("/excel/order")
//    public HttpEntity<?> getOrderList() {
//        return excelService.downloadOrderList();
//    }
//
//    //    @PreAuthorize("hasAnyAuthority('PDF_EXCEL')")
//    @GetMapping("/excel/customer")
//    public HttpEntity<?> getCustomerList() {
//        return excelService.downloadCustomerList();
//    }
//
//    //    @PreAuthorize("hasAnyAuthority('PDF_EXCEL')")
//    @GetMapping("/pdf/agent")
//    public HttpEntity<?> getAgentListPdf() throws IOException, JRException {
//        return pdfService.getAgentListPdf();
//    }

    //    @PreAuthorize("hasAnyAuthority('PDF_EXCEL')")
//    @GetMapping("/pdf/order")
//    public HttpEntity<?> getOrderListPdf() throws IOException, JRException {
//        return pdfService.getOrderListPdf();
//    }
//
//    //    @PreAuthorize("hasAnyAuthority('PDF_EXCEL')")
//    @GetMapping("/pdf/order/{id}")
//    public HttpEntity<?> getOrderListPdfByAgent(@PathVariable UUID id) throws IOException, JRException {
//        return pdfService.getOrderListPdfByAgent(id);
//    }
//
//    //    @PreAuthorize("hasAnyAuthority('PDF_EXCEL')")
//    @GetMapping("/pdf/customer")
//    public HttpEntity<?> getCustomerListPdf() throws IOException, JRException {
//        return pdfService.getCustomerListPdf();
//    }

    @GetMapping("/pdf/{id}")
    public HttpEntity<?> getFilePDF(@PathVariable UUID id, @RequestParam(value = "download") boolean download) {
        return attachmentService.getFilePDF(id, download);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable UUID id) {
        attachmentService.deleteAttachment(id);
    }


}
