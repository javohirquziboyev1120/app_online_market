package com.example.app_online_market_server.controller;

import com.example.app_online_market_server.payload.ApiResponse;
import com.example.app_online_market_server.payload.ProductDto;
import com.example.app_online_market_server.service.ProductService;
import com.example.app_online_market_server.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("api/product")
public class ProductController {
    @Autowired
    ProductService productService;

    @PreAuthorize("hasAnyAuthority('ADD_PRODUCT')")
    @PostMapping
    public HttpEntity<?> addProduct(@RequestBody ProductDto dto){
        ApiResponse apiResponse = productService.addProduct(dto);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }
    @PreAuthorize("hasAnyAuthority('EDIT_PRODUCT')")
    @PutMapping
    public HttpEntity<?> editPropduct(@RequestBody ProductDto dto){
        ApiResponse apiResponse = productService.editProduct(dto);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.CREATED: HttpStatus.CONFLICT).body(apiResponse);
    }
    @GetMapping("/getPage")
    public HttpEntity<?> getProductPage(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
                                        @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size){
        return ResponseEntity.ok(productService.getProductPage(page, size));
    }
    @GetMapping("/getProductLis")
    public HttpEntity<?> getProductList(){
        return ResponseEntity.ok(productService.getProductList());
    }
    @PreAuthorize("hasAnyAuthority('DELETE_PRODUCT')")
    @DeleteMapping
    public HttpEntity<?> deleteProduct(@RequestParam(value = "id") UUID id){
        ApiResponse apiResponse = productService.deleteProduct(id);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }


}
