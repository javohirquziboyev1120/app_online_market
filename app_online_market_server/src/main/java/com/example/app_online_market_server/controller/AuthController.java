package com.example.app_online_market_server.controller;


import com.example.app_online_market_server.entity.User;
import com.example.app_online_market_server.payload.ApiResponse;
import com.example.app_online_market_server.payload.JwtToken;
import com.example.app_online_market_server.payload.ReqLogin;
import com.example.app_online_market_server.payload.UserDto;
import com.example.app_online_market_server.repository.UserRepository;
import com.example.app_online_market_server.security.JwtTokenProvider;
import com.example.app_online_market_server.service.AuthService;
import com.example.app_online_market_server.service.CheckRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private AuthService authService;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JwtTokenProvider jwtTokenProvider;
    @Autowired
    CheckRole checkRole;
    @Autowired
    UserRepository userRepository;
    @Autowired
    PasswordEncoder passwordEncoder;


    /**
     * userlarni registratsiyadan o'tkazish uchun
     * @param request UserDto da userning ma'lumotlari keladi
     * @return ro'yxatdan o'tsa true qaytaramiz
     */
    @PostMapping("/registerUser")
    private HttpEntity<?> registerUser(@Valid @RequestBody UserDto request) {
        ApiResponse apiResponse = authService.registerUser(request);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }
    @PostMapping("/login")
    public HttpEntity<?> login(@RequestBody ReqLogin reqLogin) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                reqLogin.getPhoneNumber(),
                reqLogin.getPassword()));
        User user = (User) authentication.getPrincipal();
        String token = jwtTokenProvider.generateToken(user);
        return ResponseEntity.ok(new JwtToken(token));
    }
//    @PutMapping("/forgotPassword")
//    private HttpEntity<?> forgotPassword(@RequestParam(value = "username") String username) {
//        ApiResponse response = authService.forgotPassword(username);
//        return ResponseEntity.status(response.isSuccess() ? 201 : 409).body(response);
//    }
    @PutMapping("/resetPassword")
    private HttpEntity<?> resetPassword(@RequestParam(value = "password") String password,
                                        @RequestParam(value = "prePassword") String prePassword,
                                        @RequestParam(value = "emailCode") String emailCode) {
        ApiResponse response = authService.resetPassword(password, prePassword, emailCode);
        return ResponseEntity.status(response.isSuccess() ? 201 : 409).body(response);
    }


}
