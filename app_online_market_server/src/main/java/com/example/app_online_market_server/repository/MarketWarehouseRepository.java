package com.example.app_online_market_server.repository;

import com.example.app_online_market_server.entity.Category;
import com.example.app_online_market_server.entity.MarketWarehouse;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface MarketWarehouseRepository extends JpaRepository<MarketWarehouse, UUID> {
}
