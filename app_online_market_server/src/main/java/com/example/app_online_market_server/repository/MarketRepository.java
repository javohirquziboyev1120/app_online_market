package com.example.app_online_market_server.repository;

import com.example.app_online_market_server.entity.Market;
import com.example.app_online_market_server.projection.CustomCategory;
import com.example.app_online_market_server.projection.CustomMarket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.UUID;

@CrossOrigin
@RepositoryRestResource(path = "/market", collectionResourceRel = "list", excerptProjection = CustomMarket.class)
public interface MarketRepository extends JpaRepository<Market, UUID> {
}
