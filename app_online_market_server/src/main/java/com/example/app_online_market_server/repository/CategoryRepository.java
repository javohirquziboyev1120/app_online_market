package com.example.app_online_market_server.repository;

import com.example.app_online_market_server.entity.Category;
import com.example.app_online_market_server.entity.Maker;
import com.example.app_online_market_server.projection.CustomCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.UUID;

@CrossOrigin
@RepositoryRestResource(path = "/category", collectionResourceRel = "list", excerptProjection = CustomCategory.class)
public interface CategoryRepository extends JpaRepository<Category, UUID> {
}
