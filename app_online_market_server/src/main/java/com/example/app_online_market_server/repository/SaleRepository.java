package com.example.app_online_market_server.repository;

import com.example.app_online_market_server.entity.Detail;
import com.example.app_online_market_server.entity.Sale;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface SaleRepository extends JpaRepository<Sale, UUID> {
}
