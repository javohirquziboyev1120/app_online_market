package com.example.app_online_market_server.repository;

import com.example.app_online_market_server.entity.Detail;
import com.example.app_online_market_server.entity.Invalid;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface InvalidRepository extends JpaRepository<Invalid, UUID> {
}
