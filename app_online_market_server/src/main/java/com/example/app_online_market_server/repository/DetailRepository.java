package com.example.app_online_market_server.repository;

import com.example.app_online_market_server.entity.Category;
import com.example.app_online_market_server.entity.Detail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface DetailRepository extends JpaRepository<Detail, UUID> {
}
