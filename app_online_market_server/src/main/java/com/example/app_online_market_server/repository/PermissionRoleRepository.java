package com.example.app_online_market_server.repository;

import com.example.app_online_market_server.entity.PermissionRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

@RepositoryRestResource(path = "permissionRole", collectionResourceRel = "list")
public interface PermissionRoleRepository extends JpaRepository<PermissionRole, Integer> {

    @RestResource(path = "byRole")
    List<PermissionRole> findAllByRoleId(@Param("roleId") Integer roleId);
}
