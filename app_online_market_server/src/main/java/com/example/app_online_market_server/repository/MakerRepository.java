package com.example.app_online_market_server.repository;

import com.example.app_online_market_server.entity.Maker;
import com.example.app_online_market_server.entity.Market;
import com.example.app_online_market_server.projection.CustomCategory;
import com.example.app_online_market_server.projection.CustomMaker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.UUID;

@CrossOrigin
@RepositoryRestResource(path = "/maker", collectionResourceRel = "list", excerptProjection = CustomMaker.class)
public interface MakerRepository extends JpaRepository<Maker, UUID> {
}
