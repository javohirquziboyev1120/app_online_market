package com.example.app_online_market_server.repository;


import com.example.app_online_market_server.entity.Role;
import com.example.app_online_market_server.entity.User;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.*;

@JaversSpringDataAuditable
@EnableAutoConfiguration(exclude = HibernateJpaAutoConfiguration.class)
public interface UserRepository extends JpaRepository<User, UUID> {


    Optional<User> findByOrPhoneNumber(String phoneNumber);

}
