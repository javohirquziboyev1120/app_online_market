package com.example.app_online_market_server.repository;

import com.example.app_online_market_server.entity.Measure;
import com.example.app_online_market_server.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ProductRepository extends JpaRepository<Product, UUID> {
}
