package com.example.app_online_market_server.payload;

import com.example.app_online_market_server.entity.Category;
import com.example.app_online_market_server.entity.Maker;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {

    private UUID id;

    private String name;

    private String description;

    private UUID categoryId;

    private UUID makerId;

    private UUID measureId;

    private UUID attachmentId;

    private CategoryDto category;

    private MakerDto maker;

    private MeasureDto measureDto;

    public ProductDto(UUID id, String name, String description, CategoryDto category, MakerDto maker, UUID id1) {
        this.id=id;
        this.name=name;
        this.description=description;
        this.category=category;
        this.maker=maker;
        this.attachmentId=id1;
    }
}
