package com.example.app_online_market_server.payload;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@AllArgsConstructor
@NoArgsConstructor
public class PermissionDto {
    private String permissionName;
    private Integer id;
    private String authority;
    private String perName;
    private String generalName;

    public PermissionDto(Integer id, String perName) {
        this.id = id;
        this.perName = perName;
    }
}
