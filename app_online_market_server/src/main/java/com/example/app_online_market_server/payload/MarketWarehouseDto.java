package com.example.app_online_market_server.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketWarehouseDto {

    private UUID id;

    private UUID marketId;

    private UUID warehouseId;

    private  Integer amount;

    private Date arrivedDate;

    private  Integer price;

    private Integer leftover;
}
