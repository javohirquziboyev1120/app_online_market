package com.example.app_online_market_server.payload;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ReqLogin {
    @NotBlank
    private String phoneNumber;
    @NotBlank
    private String password;
    private boolean newUser;
}
