package com.example.app_online_market_server.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvalidDto {

    private UUID id;

    private UUID marketWarehouseId;

    private Integer amount;
    private double allPrice;

}
