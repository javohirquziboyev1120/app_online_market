package com.example.app_online_market_server.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WarehouseDto {

    private UUID id;

    private UUID productId;

    private Integer  amount;

    private Date arrivedDate;

    private double price;

    private Integer leftover;

    private ProductDto productDto;


    public WarehouseDto(UUID id, Integer amount, Integer leftover, Date arrivedDate, double price, ProductDto productDto) {
        this.id=id;
        this.amount=amount;
        this.leftover=leftover;
        this.arrivedDate=arrivedDate;
        this.price=price;
        this.productDto=productDto;
    }
}
