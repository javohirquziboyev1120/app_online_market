package com.example.app_online_market_server.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketDto {

    private UUID id;

    private String name;


    private String description;

    private UUID attachmentId;

}
