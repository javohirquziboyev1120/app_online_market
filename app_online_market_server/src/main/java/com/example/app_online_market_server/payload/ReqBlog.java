package com.example.app_online_market_server.payload;

import lombok.Data;

import java.util.UUID;

@Data
public class ReqBlog {
    private String title;
    private String text;
    private Integer categoryId;
    private String sampleText;
    private boolean featured;
    private UUID attachmentId;

    private String termsEnum;
}
