package com.example.app_online_market_server.payload;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private UUID id;
    private String firstName;
    private String lastName;

    private String password;

    private String prePassword;

    private String oldPassword;

    @NotNull
    private String phoneNumber;

    private UUID photoId;

    private UUID marketId;


    private Timestamp updatedAt;


    public UserDto(UUID id, String firstName, String lastName, String phoneNumber, String password, UUID marketId,  UUID photoId) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.password=password;
        this.marketId=marketId;
        this.photoId = photoId;

    }


    private List<Integer> permissionsId = new ArrayList<>();

    public UserDto(String firstName, String lastName, String phoneNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
    }

}
