package com.example.app_online_market_server.entity;

import com.example.app_online_market_server.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Category extends AbsNameEntity {
    @OneToOne
    private Attachment photo;//USERNING AVATAR PHOTOSI

}
