package com.example.app_online_market_server.entity;

import com.example.app_online_market_server.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.websocket.OnOpen;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class MarketWarehouse extends AbsEntity {
    @ManyToOne
    private Market market;//Qaysi marketga junatilyapdi
    @ManyToOne
    private Warehouse warehouse;//qaysi ombor mahsulotidan

    @Column(nullable = false)
    private Integer amount;//qancha junatilganligi

    private Date arrivedDate;// qaysi kuni junatildi

    private Integer price; //nechi puldan chiqdi

    private Integer leftover;// aynan ana shu marketda nechta qolganligi
}
