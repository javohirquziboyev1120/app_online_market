package com.example.app_online_market_server.entity.enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum PermissionName {

    /**
     * START DATA REST PERMISSION ENUM
     */
    //PRODUC
    ADD_PRODUCT("Add product", Arrays.asList(RoleName.ROLE_SUPER_ADMIN)),
    EIT_PRODUCT("edit product", Arrays.asList(RoleName.ROLE_SUPER_ADMIN)),
    DELETE_PRODUCT("delete product", Arrays.asList(RoleName.ROLE_SUPER_ADMIN)),



    //WEEK DAY ENTITY
    DELETE_WEEK_DAY("Delete week days", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage week day"),
    SAVE_WEEK_DAY("Add and edit sub week days", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage week day"),

    /**
     * FINISH DATA REST PERSMISSION ENUM
     */

//    VERIFY_EMAIL(Arrays.asList(RoleName.ROLE_USER, RoleName.ROLE_AGENT, RoleName.ROLE_ADMIN)),
//    GET_STATE(Arrays.asList(RoleName.ROLE_USER, RoleName.ROLE_AGENT, RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN)),
//    //    GET_COUNTY, GET_PAY_TYPE,
////    GET_COUNTRY_LIST, GET_STATE_LIST, GET_COUNTY_LIST, GET_PAY_TYPE_LIST,
////    ADD_COUNTRY, ADD_STATE, ADD_COUNTY, ADD_PAY_TYPE,
////    EDIT_COUNTRY, EDIT_STATE, EDIT_COUNTY, EDIT_PAY_TYPE,
////    DELETE_COUNTRY, DELETE_STATE, DELETE_COUNTY, DELETE_PAY_TYPE,
////    REGISTER_ROLE_USER,
////    VERIFY_EMAIL,
////    LOGIN

    //HISTORY ENTITY
    GET_HISTORY("Get history", Arrays.asList(RoleName.ROLE_SUPER_ADMIN), "Get history"),
    //{ended: Sirojiddin}

    UPLOAD_FILE_ATTACHMENT("Attachment upload file", Arrays.asList(RoleName.ROLE_USER,  RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage file attachment"),
    GET_FILE_ATTACHMENT("Attachment get file", Arrays.asList( RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage file attachment"),


    ADD_BLOG_SERVICE("Add blog", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage blog"),
    EDIT_BLOG_SERVICE("Edit blog", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage blog"),
    DELETE_BLOG_SERVICE("Delete blog", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage blog"),
    COMPLETE_ORDER("Complete order", Arrays.asList( RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage order");

    public List<RoleName> roleNames;
    public String name;
    public String generalName;

    PermissionName(String name, List<RoleName> roleNames, String generalName) {
        this.roleNames = roleNames;
        this.name = name;
        this.generalName = generalName;
    }

    PermissionName(String name, List<RoleName> roleNames) {
        this.roleNames = roleNames;
        this.name = name;
    }
}
