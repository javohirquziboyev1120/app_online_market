package com.example.app_online_market_server.entity;

import com.example.app_online_market_server.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Maker extends AbsNameEntity {
    @OneToOne
    private Attachment photo;//USERNING AVATAR PHOTOSI



}
