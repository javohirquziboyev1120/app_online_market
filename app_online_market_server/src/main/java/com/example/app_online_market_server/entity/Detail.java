package com.example.app_online_market_server.entity;

import com.example.app_online_market_server.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@EqualsAndHashCode(callSuper = true)
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Detail extends AbsNameEntity {

    @ManyToOne
    private Detail detailId;
    @OneToOne
    private Attachment photo;//USERNING AVATAR PHOTOSI

}
