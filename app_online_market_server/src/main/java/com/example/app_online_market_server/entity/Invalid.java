package com.example.app_online_market_server.entity;

import com.example.app_online_market_server.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Invalid extends AbsEntity {
    @ManyToOne
    private MarketWarehouse marketWarehouse;
    @Column(nullable = false)
    private Integer amount;//Qancha birak chiqdi
    private double allPrice;//nechi puldan sotilishi kerak edi
}
