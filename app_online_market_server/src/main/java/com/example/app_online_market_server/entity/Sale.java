package com.example.app_online_market_server.entity;

import com.example.app_online_market_server.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Sale extends AbsEntity {
    @ManyToOne
    private MarketWarehouse marketWarehouse;
    @ManyToOne
    private Region region;
    @Column(nullable = false)
    private Integer amount;//nechta sotildi
    @Column(nullable = false)
    private double price;// nechti puldan donasi

    private double allPrice;//umumiy summasi bu narsa suralmaydi
}
