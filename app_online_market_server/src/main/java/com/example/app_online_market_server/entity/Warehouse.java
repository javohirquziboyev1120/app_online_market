package com.example.app_online_market_server.entity;

import com.example.app_online_market_server.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.tomcat.jni.Proc;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Warehouse extends AbsEntity {
    @ManyToOne
    private Product product;
    @Column(nullable = false)
    private Integer amount;//nechta kirib kelgani

    private Date arrivedDate;//qachon yetib kelganligi

    @Column(nullable = false)
    private double price;//nechi puldan kirib keldi

    private Integer leftover;

}
