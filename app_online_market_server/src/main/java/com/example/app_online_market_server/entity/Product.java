package com.example.app_online_market_server.entity;

import com.example.app_online_market_server.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Product extends AbsNameEntity {
    @ManyToOne
    private Category category;
    @ManyToOne
    private Maker maker;
    @ManyToOne
    private Measure measure;
    @OneToOne
    private Attachment photo;//USERNING AVATAR PHOTOSI

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "product_detail",
            joinColumns = {@JoinColumn(name = "product_id")},
            inverseJoinColumns = {@JoinColumn(name = "detail_id")})
    private Set<Detail> details;//product detaillari

}
