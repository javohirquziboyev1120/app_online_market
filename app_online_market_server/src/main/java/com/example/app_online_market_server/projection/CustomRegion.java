package com.example.app_online_market_server.projection;

import com.example.app_online_market_server.entity.Measure;
import com.example.app_online_market_server.entity.Region;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "region", types = Region.class)
public interface CustomRegion {
    Integer getId();

    String getName();




}
