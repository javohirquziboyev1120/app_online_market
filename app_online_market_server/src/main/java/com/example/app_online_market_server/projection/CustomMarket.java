package com.example.app_online_market_server.projection;

import com.example.app_online_market_server.entity.Attachment;
import com.example.app_online_market_server.entity.Maker;
import com.example.app_online_market_server.entity.Market;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "market", types = Market.class)
public interface CustomMarket {
    Integer getId();

    String getName();

    Attachment getPhoto();

    @Value("#{target.photo.id}")
    UUID getPhotoId();


}
