package com.example.app_online_market_server.projection;

import com.example.app_online_market_server.entity.Attachment;
import com.example.app_online_market_server.entity.Maker;
import com.example.app_online_market_server.entity.Measure;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "measure", types = Measure.class)
public interface CustomMeasure {
    Integer getId();

    String getName();




}
