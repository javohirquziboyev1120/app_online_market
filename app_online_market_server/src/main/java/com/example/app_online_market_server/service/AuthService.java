package com.example.app_online_market_server.service;

import com.example.app_online_market_server.entity.User;
import com.example.app_online_market_server.entity.enums.RoleName;
import com.example.app_online_market_server.payload.ApiResponse;
import com.example.app_online_market_server.payload.UserDto;
import com.example.app_online_market_server.repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.UUID;

@Service
public class AuthService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PermissionRepository permissionRepository;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    MarketRepository marketRepository;

    public ApiResponse registerUser(UserDto userDto) {
        try {
            ApiResponse checkPassword = checkPassword(userDto);
            if (!checkPassword.isSuccess())
                return checkPassword;
            User user = makeUser(userDto, false);
            user.setPermissions(new HashSet<>(permissionRepository.findAllByRoleName(RoleName.ROLE_USER.name())));
            User save = userRepository.save(user);
            return new ApiResponse("Congratulation you are successfully registrated.", true);
        } catch (Exception e) {
            return new ApiResponse("Failed to save user", false);
        }
    }


    /**
     * userni tayyor hola yasab berish uchun
     *
     * @param userDto
     * @param admin
     * @return
     */
    public User makeUser(UserDto userDto, Boolean admin) {
        return new User(
                userDto.getFirstName(),
                userDto.getLastName(),
                userDto.getPhoneNumber(),
                new BCryptPasswordEncoder().encode(userDto.getPassword()),
                userDto.getMarketId() == null ? null : marketRepository.findById(userDto.getMarketId()).orElse(null),
                roleRepository.findAllByRoleName(
                        admin ? RoleName.ROLE_ADMIN
                                : RoleName.ROLE_USER
                ),
                userDto.getPhotoId() == null ? null : attachmentRepository.findById(userDto.getPhotoId()).orElse(null)
        );
    }


    /**
     * PAROLNI TEKSHIRADIGAN METHOD
     *
     * @param userDto
     * @return
     */
    public ApiResponse checkPassword(UserDto userDto) {
        if (userDto.getPassword().length() < 6 || userDto.getPassword().length() > 16)
            return new ApiResponse("Password size between 6 and 16 character!", false);
        if (!(userDto.getPassword().equals(userDto.getPrePassword())))
            return new ApiResponse("Password and prepassword is not equals!", false);
        return new ApiResponse("", true);
    }


    public UserDetails getUserById(UUID id) {
        return userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getUser"));
    }

    @Override
    public UserDetails loadUserByUsername(String userName) {
        return userRepository.findByOrPhoneNumber(userName).orElseThrow(() -> new UsernameNotFoundException("get user name"));
    }

    public ApiResponse resetPassword(String password, String prePassword, String phone) {
        if (!password.equals(prePassword)) {
            return new ApiResponse("Password is not matches", false);
        }
        Optional<User> userOptional = userRepository.findByOrPhoneNumber(phone);
        if (!userOptional.isPresent()) {
            return new ApiResponse("No users found", false);
        }
        userOptional.get().setPassword(password);
        userRepository.save(userOptional.get());
        return new ApiResponse("success", true);
    }
}