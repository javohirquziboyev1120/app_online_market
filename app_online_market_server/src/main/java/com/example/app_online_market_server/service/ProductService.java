package com.example.app_online_market_server.service;

import com.example.app_online_market_server.entity.Category;
import com.example.app_online_market_server.entity.Maker;
import com.example.app_online_market_server.entity.Product;
import com.example.app_online_market_server.exception.ResourceNotFoundException;
import com.example.app_online_market_server.payload.*;
import com.example.app_online_market_server.repository.*;
import org.apache.regexp.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProductService {
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    MakerRepository makerRepository;
    @Autowired
    MeasureRepository measureRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    DetailRepository detailRepository;
    @Autowired
    AttachmentRepository attachmentRepository;

    public ApiResponse addProduct(ProductDto dto){
        Product product=new Product();
        return  addOrEditProduct(dto, product);
    }
    public ApiResponse editProduct(ProductDto dto){
        Optional<Product> optionalProduct = productRepository.findById(dto.getId());
        if (optionalProduct.isPresent()){
            Product product = optionalProduct.get();
            return  addOrEditProduct(dto, product);
        }
        return new ApiResponse("Mahsulot topilmadi", false);
    }
    public ApiResponse getProductPage(Integer page, Integer size){
            Pageable pageable= PageRequest.of(page, size);
            Page<Product> productPage = productRepository.findAll(pageable);
            return new ApiResponse(true, new ResPageable(
                    page, size,
                    productPage.getTotalPages(),
                    productPage.getTotalElements(),
                    productPage.getContent().stream().map(this::getProductDto).collect(Collectors.toList())));
    }
    public ApiResponse deleteProduct(UUID id){
        productRepository.deleteById(id);
        return new ApiResponse("Mahsulot o'chirildi.", true);

    }
    public List<Product> getProductList(){
        return productRepository.findAll();
    }
    public ProductDto getProductDto(Product product){
        return new ProductDto(
                product.getId(),
                product.getName(),
                product.getDescription(),
                getCategory(product.getCategory()),
                getMaker(product.getMaker()),
                product.getPhoto().getId()
        );
    }

    public CategoryDto getCategory(Category category){
        return new CategoryDto(
                category.getId(),
                category.getName(),
                category.getDescription(),
                category.getPhoto().getId()
        );
    }

    public MakerDto getMaker(Maker maker){
        return new MakerDto(
                maker.getId(),
                maker.getName(),
                maker.getDescription(),
                maker.getPhoto().getId()
        );
    }

    public ApiResponse addOrEditProduct(ProductDto dto, Product product){
        product.setCategory(categoryRepository.findById(dto.getCategoryId()).orElseThrow(()->new ResourceNotFoundException("Category","Id", dto)));
        product.setMaker(makerRepository.findById(dto.getMakerId()).orElseThrow(()->new ResourceNotFoundException("Maker","Id", dto)));
        product.setMeasure(measureRepository.findById(dto.getMeasureId()).orElseThrow(()->new ResourceNotFoundException("MEasure ","Id", dto)));
        product.setPhoto(attachmentRepository.findById(dto.getAttachmentId()==null? null :dto.getAttachmentId()).orElseThrow(()->new ResourceNotFoundException("photo ","Id", dto)));
        product.setName(dto.getName());
        product.setDescription(dto.getDescription()!=null ? dto.getDescription() : " " );
        productRepository.save(product);
        return new ApiResponse("Mahsulot saqlandi", true);
    }
}
