package com.example.app_online_market_server.service;

import com.example.app_online_market_server.entity.Role;
import com.example.app_online_market_server.repository.RoleRepository;
import org.springframework.stereotype.Service;

import java.util.Set;

import static com.example.app_online_market_server.entity.enums.RoleName.*;

@Service
public class CheckRole {

    final
    RoleRepository roleRepository;

    public CheckRole(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public boolean isROLE_SUPER_ADMIN(Set<Role> roles) {
        for (Role role : roles) {
            if (role.getRoleName() == ROLE_SUPER_ADMIN) {
                return true;
            }
        }
        return false;
    }

    public boolean isAdmin(Set<Role> roles) {
        for (Role role : roles) {
            if (role.getRoleName() == ROLE_ADMIN ||role.getRoleName()==ROLE_SUPER_ADMIN) {
                return true;
            }
        }
        return false;
    }



    public boolean isUser(Set<Role> roles) {
        for (Role role : roles) {
            if (role.getRoleName() == ROLE_USER) {
                return true;
            }
        }
        return false;
    }

}
