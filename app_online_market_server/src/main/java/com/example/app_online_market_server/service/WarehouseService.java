package com.example.app_online_market_server.service;

import com.example.app_online_market_server.entity.Warehouse;
import com.example.app_online_market_server.exception.ResourceNotFoundException;
import com.example.app_online_market_server.payload.ApiResponse;
import com.example.app_online_market_server.payload.ResPageable;
import com.example.app_online_market_server.payload.WarehouseDto;
import com.example.app_online_market_server.repository.ProductRepository;
import com.example.app_online_market_server.repository.WarehouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class WarehouseService {
    @Autowired
    ProductRepository productRepository;
    @Autowired
    WarehouseRepository warehouseRepository;
    @Autowired
    ProductService productService;
    public ApiResponse addWarehouse(WarehouseDto dto){
        Warehouse warehouse=new Warehouse();
        return addOrEditWarehouse(dto, warehouse);
    }
    public ApiResponse editWarehouse(WarehouseDto dto){
        Optional<Warehouse> optionalWarehouse = warehouseRepository.findById(dto.getId());
        if (optionalWarehouse.isPresent()){
            Warehouse warehouse = optionalWarehouse.get();
            return addOrEditWarehouse(dto,warehouse);
        }else{
            return new ApiResponse("Bundam malumot mavjud emas", false);
        }
    }
    public ApiResponse deleteWareHouse(UUID id){
        warehouseRepository.deleteById(id);
        return  new ApiResponse("Malumot o'chirildi", true);
    }
    public ApiResponse getWarehousePage(Integer page, Integer size){
        Pageable pageable= PageRequest.of(page, size);
        Page<Warehouse> warehousePage=warehouseRepository.findAll(pageable);
        return new ApiResponse(true,
        new ResPageable(
                page, size,
                warehousePage.getTotalPages(),
                warehousePage.getTotalElements(),
                warehousePage.getContent().stream().map(this::getWarehouseDto).collect(Collectors.toList())));
    }

    public WarehouseDto getWarehouseDto(Warehouse warehouse){
        return new WarehouseDto(
                warehouse.getId(),
                warehouse.getAmount(),
                warehouse.getLeftover(),
                warehouse.getArrivedDate(),
                warehouse.getPrice(),
                productService.getProductDto(warehouse.getProduct())
        );
    }
    public List<Warehouse> getList(){
        return warehouseRepository.findAll();
    }


    public ApiResponse addOrEditWarehouse(WarehouseDto dto, Warehouse warehouse){
        warehouse.setAmount(dto.getAmount());
        warehouse.setLeftover(dto.getAmount());
        warehouse.setArrivedDate(dto.getArrivedDate());
        warehouse.setPrice(dto.getPrice());
        warehouse.setProduct(productRepository.findById(dto.getProductId()).orElseThrow(()->new ResourceNotFoundException("Product", "id",dto)));
        warehouseRepository.save(warehouse);
        return new ApiResponse("Omborga mahsulot qo'shildi", true);

    }
}
