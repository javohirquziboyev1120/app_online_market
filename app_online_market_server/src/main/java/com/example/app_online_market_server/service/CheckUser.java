package com.example.app_online_market_server.service;

import com.example.app_online_market_server.entity.Role;
import com.example.app_online_market_server.entity.User;
import com.example.app_online_market_server.entity.enums.RoleName;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class CheckUser {

    public boolean isSuperAdmin(User user) {
        return checkRole(RoleName.ROLE_SUPER_ADMIN, user);
    }

    public boolean isAdmin(User user) {
        return checkRole(RoleName.ROLE_ADMIN, user);
    }


    public boolean isUser(User user) {
        return checkRole(RoleName.ROLE_USER, user);
    }

    private boolean checkRole(RoleName roleName, User user){
        Set<Role> roles = user.getRoles();
        for (Role role : roles) {
            if (role.getRoleName().equals(roleName)){
                return true;
            }
        }
        return false;
    }
}
