package com.example.app_online_market_server.utils;


import com.example.app_online_market_server.exception.BadRequestException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CommonUtils {
    public static void validatePageNumberAndSize(int page, int size) {
        if (page < 0) {
            throw new BadRequestException("Sahifa soni noldan kam bo'lishi mumkin emas.");
        }

        if (size > AppConstants.MAX_PAGE_SIZE * 10) {
            throw new BadRequestException("Sahifa soni " + AppConstants.MAX_PAGE_SIZE + " dan ko'p bo'lishi mumkin emas.");
        }
    }

    public static String changeTimeAmericanStn(Date date) {
        return new SimpleDateFormat("hh:mm a", Locale.US).format(date);
    }


    public static Pageable getPageable(int page, int size) {
        validatePageNumberAndSize(page, size);
        return PageRequest.of(page, size, Sort.Direction.ASC, "id");
    }

    public static Pageable getPageableForNative(int page, int size) {
        validatePageNumberAndSize(page, size);
        return PageRequest.of(page, size);
    }

    public static Pageable getPageable(int page, int size, Sort.Direction direction, String... field) {
        validatePageNumberAndSize(page, size);
        return PageRequest.of(page, size, direction, field);
    }


    public static Pageable getPageableForNative(int page, int size, Sort.Direction direction, String... field) {
        validatePageNumberAndSize(page, size);
        return PageRequest.of(page, size, direction, field);
    }


    public static String thousandSeparator(Integer a) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();

        symbols.setGroupingSeparator(' ');
        formatter.setDecimalFormatSymbols(symbols);
        return formatter.format(a.longValue());
    }

    public static double decimalFormat(double sum) {
        String pattern = "#.##";
        DecimalFormat decimalFormat = new DecimalFormat(pattern);
        String format = decimalFormat.format(sum);
        return Double.parseDouble(format);
    }

}
