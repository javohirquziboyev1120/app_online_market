package com.example.app_online_market_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppOnlineMarketServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppOnlineMarketServerApplication.class, args);
    }

}
